import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TrackingModule } from 'projects/ipa-module/tracking/src/public-api';
import { environment } from "../environments/environment";

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    TrackingModule.forRoot(environment)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
