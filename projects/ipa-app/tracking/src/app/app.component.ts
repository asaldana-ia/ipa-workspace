import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-tracking-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AppComponent {
  title = '@ipa-app/tracking';
}
