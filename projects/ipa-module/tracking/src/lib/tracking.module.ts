import { ModuleWithProviders, NgModule } from '@angular/core';
import { TrackingComponent } from './tracking.component';
import { TrackingService } from './tracking.service';

@NgModule({
  declarations: [
    TrackingComponent
  ],
  imports: [
  ],
  exports: [
    TrackingComponent
  ]
})

export class TrackingModule {

  public static forRoot(environment: any): ModuleWithProviders<TrackingModule> {
    return {
      ngModule: TrackingModule,
      providers: [
        /** TODO: Replace with iaApi service */
        TrackingService,
        { provide: 'env', useValue: environment }
      ]
    };
  }

}
