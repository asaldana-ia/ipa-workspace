import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrackingService {

  constructor(@Inject('env') private env) { }

  getEnv() {
    return this.env;
  }
}
