import { Component, OnInit } from '@angular/core';
import { TrackingService } from './tracking.service';

@Component({
  selector: 'tracking',
  template: `
    <p>
      {{ env }}
    </p>
  `,
  styles: [
  ]
})
export class TrackingComponent implements OnInit {
  env = '';

  constructor(
    private tracking: TrackingService,
  ) { }

  ngOnInit(): void {
    this.env = JSON.stringify(this.tracking.getEnv());
  }

}
