## Crear modulo
- ng generate library @ipa-module/tracking
- configurar método forRoot con las configuraciones que se deseen pasar desde la app (environments por ejemplo) [forRoot](https://angular.io/guide/singleton-services#the-forroot-pattern)


# Crear nueva app
- ng generate application @ipa-app/tracking --prefix app-tracking
- ng add ngx-build-plus --project @ipa-app/tracking
- eliminar safari del browserlist por que si no da este error al hacer build:
```
An unhandled exception occurred: Transform failed with 1 error:
error: Invalid version: "15.2-15.3"
See "/tmp/ng-2p9cpu/angular-errors.log" for further details.
```

- Encapsular app: `encapsulation: ViewEncapsulation.ShadowDom`
- TODO environments
- instanciar módulo y pasar environments y otras configuraciones a nivel de app:




